let flats = [
    {
        bedroom: 'Studio',
        types: [
            {
                type: 'Type 1',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Type 2',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
            {
                type: 'Type 3',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Type 4',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
            {
                type: 'Type 5',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
        ]
    },


    {
        bedroom: '1',
        types: [
            {
                type: 'Type 1',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
            {
                type: 'Type 2',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
        ]
    },

    {
        bedroom: '2',
        types: [
            {
                type: 'Type 3',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Type 4',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
        ]
    },

    {
    bedroom: '3',
        types: [
            {
                type: 'Type 1',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
        ]
    }
];


let floors = [
    {
        building: 'Building 1',
        types: [{
                type: 'Ground floor',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Podium floor',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
            {
                type: 'Floors 1-7',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Floors 8-14',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
            {
                type: 'Floors 15-24',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Floor 25',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
        ]
    },
    {
        building: 'Building 2',
        types: [{
                type: 'Ground floor',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
            {
                type: 'Podium floor',
                image: 'images/__content/pages/project/constructor/floor.jpg'
            },
        ]
    },

    {
        building: 'Building 3',
        types: [{
                type: 'Ground floor',
                image: 'images/__content/pages/project/constructor/flat.jpg'
            },
        ]
    },


]






$(function () {

    function createBedroomList(){
        let html =''
        let i = 0;
        flats.forEach(flat => {
            let checked = i==0 ? 'checked ':'';
            html +=`
                <label class="project_constructor__block_x__item">
                    <input type="radio" name="Bedrooms" value="${flat.bedroom}" ${checked}>
                    <div class="project_constructor__block_x__item__view">${flat.bedroom}</div>
                </label>
            `;
            i++;
        });

        $('.js_project_constructor__block_x--bedroom').html(html)
    }

    function setBedroomItem(bedroom){
        let flat = flats.filter(function(flat) {
           if (flat.bedroom != bedroom)
              return false;
          return true;
        });

        let html =''
        let i = 0;
        flat[0].types.forEach(flat => {
            let checked = i==0 ? 'checked ':'';
            html +=`
                <label class="project_constructor__block_x__item">
                    <input type="radio" name="Layout_type" value="${flat.type}" ${checked}>
                    <div class="project_constructor__block_x__item__view">${flat.type}</div>
                </label>
            `;
            i++;
        });
        $('.js_project_constructor__block_x--type').html(html)
    }


    function setTypeImage(type){
        let imgUrl;
        let bedroom = $('.js_project_constructor__block_x--bedroom input:checked').val();

        let flat = flats.filter(function(flat) {
            if (flat.bedroom != bedroom)
               return false;
           return true;
         });

         flat[0].types.forEach(flat => {
            if (flat.type == type){
                imgUrl = flat.image
            }
        });


        $('.js_project_constructor__image').attr('data-src',imgUrl)
        $('.js_project_constructor__image img').attr('src',imgUrl)
    }

    $(document).on('change','.js_project_constructor__block_x--bedroom input', function () {
        setBedroomItem($(this).val())
        setTypeImage($('.js_project_constructor__block_x--type input').first().val())
    });


    $(document).on('change','.js_project_constructor__block_x--type input', function () {
        setTypeImage($(this).val())
    });




    function createBuildingList(){
        let html = '';
        let i = 0;


        floors.forEach(floor => {
            let selected = i==0 ? ' ':'';
            html +=`
                <option value="${floor.building}" ${selected}>${floor.building}</option>
            `;
            i++;
        });


        if ($('.js_building').hasClass('select2-hidden-accessible')){
            $('.js_building').select2('destroy');
        }

        $('.js_building').html(html)

        let placeholder = $('.js_building').attr('placeholder');

        let selectBuilding = $('.js_building').select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            allowClear: true
        })

        selectBuilding.on("change", function (e) {
            setBuildingFloor($('.js_building').val())
        });



    }

    function setBuildingFloor(building){
        let floor = floors.filter(function(floor) {
            if (floor.building != building)
               return false;
           return true;
         });



         let html =''
         let i = 0;
         floor[0].types.forEach(item => {
             let checked = i==0 ? 'checked ':'';
             html +=`
                 <label class="project_constructor__block_x__item">
                     <input type="radio" name="Layout_type" value="${item.type}" ${checked}>
                     <div class="project_constructor__block_x__item__view">${item.type}</div>
                 </label>
             `;
             i++;
         });
         $('.js_project_constructor__block_x__list--floor').html(html)
    }


    $('.js_project_constructor .project_constructor__top button').click(function(){
        if (!$(this).hasClass('active')){
            let block = $(this).closest('.js_project_constructor');
            let ind = $(this).index()

            block.find('.project_constructor__top button.active').removeClass('active')
            $(this).addClass('active')

            block.find('.project_constructor__block.active').fadeOut(400,function(){
                $(this).removeClass('active')
                block.find('.project_constructor__block').eq(ind).fadeIn(400,function(){
                    $(this).addClass('active')
                    setTypeImage($('.js_project_constructor__block_x--type input').first().val())
                })
            })

        }
    })

    function setTypeImageFloor(type){
        let imgUrl;
        let building = $('.js_building').val();


        let floor = floors.filter(function(floor) {
            if (floor.building != building)
               return false;
           return true;
         });


         console.log(floor)


         floor[0].types.forEach(floor => {
            if (floor.type == type){
                imgUrl = floor.image
            }
        });


        $('.js_project_constructor__image').attr('data-src',imgUrl)
        $('.js_project_constructor__image img').attr('src',imgUrl)
    }


    $(document).on('change','.js_project_constructor__block_x__list--floor input', function () {
        setTypeImageFloor($(this).val())
    });



    if ($('.js_project_constructor').length){
        createBedroomList()
        setBedroomItem(flats[0].bedroom)
        setTypeImage($('.js_project_constructor__block_x--type input').first().val())

        createBuildingList()
        setBuildingFloor(floors[0].building)
    }
});