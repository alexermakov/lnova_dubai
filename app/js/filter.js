$(function () {


    $('.js_filter_x_top input').change(function(){
        let type_id = $(this).val() // 0 buy; 1 rent
        console.log(type_id)

        if (type_id==0){
            $('.js_only_rent').addClass('--hide')
            $('.js_only_buy').removeClass('--hide')

        }else{
            $('.js_only_rent').removeClass('--hide')
            $('.js_only_buy').addClass('--hide')
        }
    })

    $('.js_filter_price__view').click(function(){
        $(this).closest('.js_filter_price').toggleClass('active')
        $('.js_filter_price__modal').toggleClass('active')
    })


    $('.js_filter_bedrooms_view').click(function(){
        $(this).closest('.js_filter_bedrooms').toggleClass('active')
        $('.js_filter_bedrooms_view__modal').toggleClass('active')
    })

    $('.js_filter_price__modal__top .filter_price__tab').click(function(){
        if (!$(this).hasClass('active')){
            $('.js_filter_price__modal__top .filter_price__tab').removeClass('active')
            $(this).addClass('active')
        }
    })


    $('body').click(function (event) {
		if ((!$(event.target).closest('.js_filter_price').length && $('.js_filter_price').hasClass('active'))){
			$('.js_filter_price').removeClass('active')
			$('.js_filter_price__modal').removeClass('active')
		}
	})


    $('body').click(function (event) {
		if ((!$(event.target).closest('.js_filter_bedrooms').length && $('.js_filter_bedrooms').hasClass('active'))){
			$('.js_filter_bedrooms').removeClass('active')
			$('.js_filter_bedrooms_view__modal').removeClass('active')
		}
	})

    $('body').click(function (event) {
		if ((!$(event.target).closest('.js_filter_type').length && $('.js_filter_type').hasClass('active'))){
			$('.js_filter_type').removeClass('active')
			$('.js_filter_type_view__modal').removeClass('active')
		}
	})

    function filterBedrooms() {
        $('.js_filter_bedrooms_view__modal input').change(function (e) {
            e.preventDefault();
            let choosedValues = '';
            $('.js_filter_bedrooms_view__modal input:checked').each(function (index, element) {
                choosedValues += $(this).val()+', ';
            });
            if (choosedValues.length>0){
                choosedValues = choosedValues.slice(0, -2);
                $('.js_filter_bedrooms_view').addClass('choosed')
            }
            else{
                choosedValues = $('.js_filter_bedrooms_view__text').data('text')
                $('.js_filter_bedrooms_view').removeClass('choosed')
            }
            $('.js_filter_bedrooms_view__text').text(choosedValues)
        });
    }







    $('.js_filter_type__modal__top a').click(function(e){
        e.preventDefault()
        if (!$(this).hasClass('active')){
            $('.js_filter_type__modal__top a').removeClass('active')
            $(this).addClass('active');
            let ind = $(this).index();
            $('.js_filter_type__modal__list__wrap .filter_type__modal__list.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_filter_type__modal__list__wrap .filter_type__modal__list').eq(ind).fadeIn(400,function(){
                    $(this).addClass('active')
                })
            })
        }
    })

    $('.js_filter_type__view').click(function(){
        $(this).closest('.js_filter_type').toggleClass('active')
        $('.js_filter_type_view__modal').toggleClass('active')
    })


    $('.filter_type__modal__list input').change(function (e) {
        e.preventDefault();
        $('.js_filter_type_view__text').text( $(this).val())
        $(this).closest('.js_filter_type').removeClass('active')
        $('.js_filter_type_view__modal').removeClass('active')
        $('.js_filter_type__view').addClass('choosed')


    })



    $('.js_btn_project_object__top_filter').click(function(){
        $('.js_project_object_top').toggleClass('active');
    })




    function filterPrice() {
        var sliderPrice = document.querySelector('.js_filter_price__modal__content__range');
        let min = parseInt($(sliderPrice).attr('data-min'));
        let max = parseInt($(sliderPrice).attr('data-max'));
        let step = parseInt($(sliderPrice).attr('data-step'));

        noUiSlider.create(sliderPrice, {
            connect: true,
            step: step,
            start: [min, max],
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                thousand: '',
                decimals: 0
            })
        });


        $(document).on('blur', '.js_filter_price__modal input[name="price_from"]', function () {
            sliderPrice.noUiSlider.set([$(this).val(),$('.js_filter_price__modal input[name="price_to"]').val()]);
        })

        $(document).on('blur', '.js_filter_price__modal input[name="price_to"]', function () {
            sliderPrice.noUiSlider.set([$('.js_filter_price__modal input[name="price_from"]').val(),$(this).val()]);
        })


        sliderPrice.noUiSlider.on('update', function (values, handle) {
            let priceRange = sliderPrice.noUiSlider.get();
            let priceText = '';
            priceText += priceRange[0]+ ' - '+ priceRange[1] +' '+$('.js_currency_value').val()
            $('.js_filter_price__view__text').text(priceText)
            $('.js_filter_price__modal input[name="price_from"]').val(priceRange[0])
            $('.js_filter_price__modal input[name="price_to"]').val(priceRange[1])
        });
    }



    function filterModalWithRange(block) {

        block.find('.js_filter_modal_range__view').click(function(){
            $(this).closest('.js_filter_modal_range').toggleClass('active')
            $(this).closest('.js_filter_modal_range').find('.js_filter_modal_range__modal').toggleClass('active')
        })

        var rangeEL = block.find('.js__drop_range')[0];
        let min = parseInt($(rangeEL).attr('data-min'));
        let max = parseInt($(rangeEL).attr('data-max'));
        let step = parseInt($(rangeEL).attr('data-step'));

        noUiSlider.create(rangeEL, {
            connect: true,
            step: step,
            start: [min, max],
            range: {
                'min': min,
                'max': max
            },
            format: wNumb({
                thousand: '',
                decimals: 0
            })
        });


        block.find('.js__drop_range__from').on('blur', function () {
            rangeEL.noUiSlider.set([$(this).val(), block.find('.js__drop_range__to')]);
        });
        block.find('.js__drop_range__to').on('blur', function () {
            rangeEL.noUiSlider.set([block.find('.js__drop_range__from').val(),$(this).val()]);
        })



        rangeEL.noUiSlider.on('update', function (values, handle) {
            let rangeValue = rangeEL.noUiSlider.get();
            let rangeText = '';
            rangeText += rangeValue[0]+ ' - '+ rangeValue[1] + ' ' + block.data('postfix')

             block.find('.js_filter_modal_range__view__text').text(rangeText)
             block.find('.js__drop_range__from').val(rangeValue[0])
             block.find('.js__drop_range__to').val(rangeValue[1])
        });


    }


    function dropWithCheckbox(el){
        el.find('.js_filter_dropWithCheckbox__view').click(function(){

            let block = $(this).closest('.js_filter_dropWithCheckbox');
            let modal = block.find('.js_filter_dropWithCheckbox__modal')

            block.toggleClass('active')
            modal.toggleClass('active')


            el.closest('.js_filter_dropWithCheckbox').find('.js_filter_dropWithCheckbox__modal input').change(function (e) {
                e.preventDefault();
                let choosedValues = '';
                modal.find('input:checked').each(function (index, element) {
                    choosedValues += $(this).val()+', ';
                });
                if (choosedValues.length>0){
                    choosedValues = choosedValues.slice(0, -2);
                    block.find('.js_filter_dropWithCheckbox__view').addClass('choosed')
                }
                else{
                    choosedValues = block.find('.js_filter_dropWithCheckbox__view__text').data('text')
                    block.find('.js_filter_dropWithCheckbox__view').removeClass('choosed')
                }
                block.find('.js_filter_dropWithCheckbox__view__text').text(choosedValues)
            });
        })

    }




    $('body').click(function (event) {
		if ((!$(event.target).closest('.js_filter_modal_range').length && $('.js_filter_modal_range').hasClass('active'))){
			$('.js_filter_modal_range').removeClass('active')
			$('.js_filter_modal_range__modal').removeClass('active')
		}
	})


    $('body').click(function (event) {
        if ((!$(event.target).closest('.js_filter_dropWithCheckbox').length && $('.js_filter_dropWithCheckbox').hasClass('active'))){
            $('.js_filter_dropWithCheckbox').removeClass('active')
            $('.js_filter_dropWithCheckbox__modal').removeClass('active')
        }
    })

    if ($('.js_filter_dropWithCheckbox').length){
        $('.js_filter_dropWithCheckbox').each(function (index, element) {
            dropWithCheckbox($(this))
        });
    }




    if ($('.js_filter_modal_range').length){
        $('.js_filter_modal_range').each(function (index, element) {
            filterModalWithRange($(this))
        });

    }

    if ($('.js_filter_price__modal__content__range').length) filterPrice()
    if ($('.js_filter_bedrooms_view__modal').length) filterBedrooms()
});