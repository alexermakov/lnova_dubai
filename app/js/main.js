function getBodyScrollTop() {
    return self.pageYOffset || (document.documentElement && document.documentElement.ScrollTop) || (document.body && document.body.scrollTop);
}

window.onload = function () {
    $('body').addClass('loaded')
    $('.js_grid').masonry({
        itemSelector: '.grid-item',
        horizontalOrder: true
    });
    $('.js_grid_0').masonry({
        itemSelector: '.grid-item',
    });


}

$(function () {



    $('.js_project__main_plan__top a').click(function(e){
        e.preventDefault()
        if (!$(this).hasClass('active')){
            ind = $(this).index()
            $('.js_project__main_plan__top a.active').removeClass('active')
            $(this).addClass('active')
            $('.js_project__main_plan .project__main_plan__block__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_project__main_plan .project__main_plan__block__item').eq(ind).fadeIn(400,function(){
                    $(this).addClass('active')
                })
            })
        }
    })




    $(".js_select").each(function () {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            allowClear: true
        })
    });

    Fancybox.bind(".js__modal", {
        autoFocus: false,
        dragToClose: false,
        trapFocus: false,
        closeButton: "inside"
    })


    Fancybox.bind("[data-fancybox='object_gallery']", {
        Thumbs: {
          autoStart: false,
        },
      });





    $('.js_project__stat__item__diagram').each(function () {
        let el = $(this).find('canvas')[0];

        let config = {
            type: "doughnut",
            data: {
                datasets: [{
                    data: [parseInt($(this).data('sold')), parseInt($(this).data('sale'))],
                    backgroundColor: ["#F2F5FB", "#1864CE"]
                }],
                hoverOffset: 0,
                hoverBorderWidth: false,
                hoverBorderColor: false
            },
            options: {
                hover: {
                    mode: null
                },
                cutout: '60%',
                plugins: {
                    tooltip: {
                        enabled: false
                    }
                }
            }
        }

        new Chart(el, config)
    })

    const ctx = document.getElementById('myChart');



    $('.js_card__btn_y .card__btn_y--favorite').click(function (e) {
        e.preventDefault()
        $(this).toggleClass('favorited')
    })

    $('.js_project__main_plan__point__item .project__main_plan__point_icon').click(function(){
        if (!$(this).closest('.js_project__main_plan__point__item').hasClass('active')){
            $('.js_project__main_plan__point__item.active').removeClass('active')


            let bgImage = $('.js_project__main_plan__image__bg');
            let blockX = $(this).closest('.js_project__main_plan__point__item')
            let leftPos = parseInt(blockX.css('left')) ;

            if (leftPos + blockX.find('.project__main_plan__point_info').outerWidth()/2>bgImage.width()){
                $(this).closest('.js_project__main_plan__point__item').addClass('plan__point__item--right')
            }
            if (leftPos - parseInt(blockX.find('.project__main_plan__point_info').outerWidth()/2) < 0){
                $(this).closest('.js_project__main_plan__point__item').addClass('plan__point__item--left')
            }

            $(this).closest('.js_project__main_plan__point__item').toggleClass('active')
        }
        else{
            $('.js_project__main_plan__point__item.active').removeClass('active')
        }
    })

    $('.js_project__main_plan__mobile_view').bind('touchstart mousedown', function(e){
		$('.js_project__main_plan__mobile_view').fadeOut(250, function () {
			$(this).remove()
		});
	});



    $('.js_btn_filter__project_card').click(function(){
        $('.js_filter__project_card').toggleClass('active')
    })

    if ($('.js_show_fixed_top').length){
        $(window).scroll(function(){
            if ($('.js_show_fixed_top').offset().top < $(window).scrollTop()){
                $('.js_fixed_top').addClass('active')
            }
            else{
                $('.js_fixed_top').removeClass('active')
            }
        })
    }

    $('.js_card_view_type .card_view_type_x').click(function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_card_view_type .card_view_type_x.active').removeClass('active')
            $(this).addClass('active')
            $('.js_project_object__list').removeClass('__grid __list')
            $('.js_project_object__list').addClass($(this).data('class_x'))
            $('.js_card_slider_image').slick('slickGoTo',0);
        }
    })

    $(window).resize(function () {
        if ($(window).width() <= 1400) {
            if ($('.js_project_object__list').length > 0) {
                $('.js_project_object__list').removeClass('__grid __list')
                $('.js_card_view_type .card_view_type_x').first().trigger('click')
            }
        }
    });



    $('body').click(function (event) {
        if ((!$(event.target).closest('.js_modal_menu').length && $('.js_modal_menu').hasClass('active')) && (!($(event.target).closest('.js_modal_menu_btn').length))) {
            $('.js_modal_menu').removeClass('active')
            $('.js_modal_menu_btn').removeClass('active')
            bodyLock()
        }
    })





    $('.js_favorite__tab a').click(function () {
        if (!$(this).hasClass('active')) {
            $('.js_favorite__tab a.active').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index();
            $('.js_favorite__content .favorite__content__item.active').stop().fadeOut(250, function () {
                $(this).removeClass('active')
                $('.js_favorite__content .favorite__content__item').eq(ind).stop().fadeIn(250, function () {
                    $(this).addClass('active')
                })
            });
        }
    })


    $('.js_btn_modal__menu__hassubmenu').click(function (e) {
        e.preventDefault()
        $(this).closest('.js_modal__menu__hassubmenu').toggleClass('active')
        $(this).closest('.js_modal__menu__hassubmenu').find('.modal__menu__sub').slideToggle(400)
    })



    $('.js_filter_x_top button').click(function () {
        $('.js_filter_x_top button.active').removeClass('active')
        $(this).addClass('active')
    })


    $('.js_home_type button').click(function () {
        if (!$(this).hasClass('active')) {
            $('.js_home_type button.active').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index();
            $('.js_home__type__list .home__type__section.active').stop().fadeOut(250, function () {
                $('.js_home__type__list .home__type__section.active').removeClass('active')
                $('.js_home__type__list .home__type__section').eq(ind).stop().fadeIn(250, function () {
                    $(this).addClass('active')
                })
            });
        }
    })


    $('.js_menu_location_btn').click(function (e) {
        e.preventDefault()
        let hasActiveClass = $(this).hasClass('active');
        $('.js_modal_menu').removeClass('active')
        $('.js_modal_menu_btn').removeClass('active')

        if (!hasActiveClass) {
            $('.js_menu_country').addClass('active')
            $(this).addClass('active')
        } else {
            $('.js_menu_country').removeClass('active')
        }
    })

    $('.js_menu_settings_btn').click(function (e) {
        e.preventDefault()
        let hasActiveClass = $(this).hasClass('active');
        $('.js_modal_menu').removeClass('active')
        $('.js_modal_menu_btn').removeClass('active')

        if (!hasActiveClass) {
            $('.js_menu_settings').addClass('active')
            $(this).addClass('active')
        } else {
            $('.js_menu_settings').removeClass('active')
        }
    })





    $('.js_menu_rent_btn').click(function (e) {
        e.preventDefault()
        let hasActiveClass = $(this).hasClass('active');
        $('.js_modal_menu').removeClass('active')
        $('.js_modal_menu_btn').removeClass('active')

        if (!hasActiveClass) {
            $('.js_menu_rent').addClass('active')
            $(this).addClass('active')
        } else {
            $('.js_menu_rent').removeClass('active')
        }
    })

    $('.js_menu_buy_btn').click(function (e) {
        e.preventDefault()
        let hasActiveClass = $(this).hasClass('active');
        $('.js_modal_menu').removeClass('active')
        $('.js_modal_menu_btn').removeClass('active')

        if (!hasActiveClass) {
            $('.js_menu_buy').addClass('active')
            $(this).addClass('active')
        } else {
            $('.js_menu_buy').removeClass('active')
        }
    })

    $('.js_menu_settings__top a').click(function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_menu_settings__top a').removeClass('active')
            $(this).addClass('active')
            let ind = $(this).index();
            $('.js_menu_settings__content .menu_settings__content__item.active').stop().fadeOut(400, function () {
                $(this).removeClass('active')
                $('.js_menu_settings__content .menu_settings__content__item').eq(ind).stop().fadeIn(400, function () {
                    $(this).addClass('active')
                })
            })
        }
    })

    function bodyLock() {
        let topY = getBodyScrollTop();
        if (!$('body').hasClass('body-lock')) {
            $('body').data('topY', topY);
            $('body').css('top', '0px');
            $('body').addClass('body-lock');
        } else {
            $('body').removeClass('body-lock');
            $('body').removeAttr('style');
            window.scrollTo(0, $('body').data('topY'));

        }
    }

    $('.js_modal_menu_btn').click(function () {
        bodyLock()
    })


    $('.js_btn_menu_close').click(function (e) {
        e.preventDefault()
        $('.js_modal_menu_btn').removeClass('active')
        $('.js_modal_menu').removeClass('active')
        if ($(window).width() <= 750) {
            let topY = getBodyScrollTop();
            if (!$('body').hasClass('body-lock')) {
                $('body').data('topY', topY);
                $('body').css('top', '0px');
                $('body').addClass('body-lock');
            } else {
                $('body').removeClass('body-lock');
                $('body').removeAttr('style');
                window.scrollTo(0, $('body').data('topY'));

            }
        }
    })




    $('.js_project__promo__slider').slick({
        infinite: false,
        prevArrow: $('.js_project__promo__arrows button:first-child').first(),
        nextArrow: $('.js_project__promo__arrows button:last-child').first(),
        responsive: [{
            breakpoint: 751,
            settings: {
                arrows: false,
                dots: true
            }
        }]
    })


    $('.js_object__promo__slider').slick({
        infinite: false,
        dots: true

    })



    $('.js_project__advantage__list').slick({
        variableWidth: true,
        swipeToSlide: true,
        responsive: [{
            breakpoint: 751,
            settings: {
                arrows: false,
            }
        }]
    })









    $('.js_btn_menu,.js_modal_overlay_menu,.js_btn_menu__close').click(function (e) {
        e.preventDefault()

        let topY = getBodyScrollTop();

        if (!$('.js_modal__menu').hasClass('active')) {
            $('body').data('topY', topY);
            $('body').css('top', '0px');
            $('body').addClass('body-lock');
        } else {
            $('body').removeClass('body-lock');
            $('body').removeAttr('style');
            window.scrollTo(0, $('body').data('topY'));

        }
        $(this).toggleClass('active')
        $('.js_modal__menu').toggleClass('active')
        $('.js_modal_overlay_menu').toggleClass('active')
    })


    $('.js_main_footer__menu__btn').click(function () {
        $(this).closest('.main_footer__menu__col').toggleClass('active').find('.main_footer__menu__list').slideToggle(400)
    })


    $('.js_project__stat__list').slick({
        variableWidth: true,
        swipeToSlide: true,
        arrows:false,
        touchThreshold: 25,
    })

    $('.js_btn_developer__map').click(function () {
        $(this).toggleClass('active')
        let $map = $('.js_map_develop');
        let $catalog = $('.js_catalog_develop');
        if ($map.hasClass('_hide_')) {

            $(this).text($(this).attr('data-textactive'))
            $map.slideDown(500, function () {
                $(this).removeClass('_hide_')
            });
            $catalog.slideUp(500, function () {
                $(this).addClass('_hide_')
            });
        } else {
            $(this).text($(this).attr('data-textnoactive'))
            $catalog.slideDown(500, function () {
                $(this).removeClass('_hide_')
            });
            $map.slideUp(500, function () {
                $(this).addClass('_hide_')
            });
        }

    })



    function initDevelopersMap() {

        const centerCoord = {
            lat: parseFloat($('.js_developer_map').attr('data-lat')),
            lng: parseFloat($('.js_developer_map').attr('data-lng'))
        };


        const map = new google.maps.Map(document.querySelector(".js_developer_map"), {
            center: centerCoord,
            styles: [
                {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "poi.attraction",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.government",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.place_of_worship",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.school",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.sports_complex",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
            ],
            zoom: 11
        });

        $('.js_developer_map__info .location').each(function () {
            let coord = {
                lat: parseFloat($(this).attr('data-lat')),
                lng: parseFloat($(this).attr('data-lng'))
            }

            console.log(coord)

            marker = new google.maps.Marker({
                position: coord,
                title: $(this).attr('data-title'),
                map: map,
                icon: 'images/icons/markerImage.svg',
            });
            marker['id'] = $(this).attr('data-id');

            google.maps.event.addListener(marker, 'click', function() {
                alert(this.id);
                // load content

                $('.js_developer_map__info .js_card_slider_image').slick('slickGoTo', 0);
                $('.js_developer_map__info').addClass('active')
            });
        });
    }

    $('.js_developer_map__info__close').click(function(){
        $('.js_developer_map__info').removeClass('active')
    })

    if ($('.js_developer_map').length){
        initDevelopersMap()
    }






});