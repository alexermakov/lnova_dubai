$(document).ready(function () {
    const mapStyle = [
        {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "poi.attraction",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.government",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.medical",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.place_of_worship",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.school",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.sports_complex",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ]


    function projectMap() {

        let map;
        let service;
        let markersX = [];

        let el = $('.js_object_map');
        const centerCoord = {
            lat: parseFloat(el.attr('data-lat')),
            lng: parseFloat(el.attr('data-lng'))
        };


        map = new google.maps.Map(el[0], {
            center: centerCoord,
            styles: mapStyle,
            zoom: 12,
            mapTypeControl: false,
            zoomControl: false,
            // draggable: false,
            scaleControl: false,
            // scrollwheel: false,
            navigationControl: false,
            streetViewControl: false,
        });

        new google.maps.Marker({
            position: centerCoord,
            map,
            icon: 'images/icons/markerImage.svg',
        });






        function clearMarkers() {
            for (var i = 0; i < markersX.length; i++) {
                markersX[i].setMap(null);
            }
            markersX = [];
        }

        function createMarker(place, iconURL) {
            if (!place.geometry || !place.geometry.location) return;

            let marker = new google.maps.Marker({
                map,
                position: place.geometry.location,
                icon: iconURL
            });
            markersX.push(marker)

        }

        $('.js_object_map__points .object_map__point__item').click(function () {
            $('.js_object_map__points .object_map__point__item').removeClass('active')
            $(this).addClass('active')
            const iconURL = $(this).data('icon');
            clearMarkers()

            var request = {
                location: centerCoord,
                radius: '500',
                query: $(this).data('request')
            };

            service = new google.maps.places.PlacesService(map);
            service.textSearch(request, callback);

            function callback(results, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        createMarker(results[i], iconURL);
                    }
                }
            }
        })

        $('.js_object_map__points .object_map__point__item').first().trigger('click')

    }

    if ($('.js_object_map').length){
        projectMap()
    }
});