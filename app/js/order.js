$(document).ready(function () {
    $('.js_order_x').click(function(e){
        e.preventDefault()

        let $thisOrderEl = $(this)

        $('.js_order__list .js_order_x').each(function () {
            if ($(this)[0] != $thisOrderEl[0]){
                $(this).removeClass('sort-asc sort-desc')
                $(this).find('input').removeAttr('checked')
                $(this).find('input').first().attr('checked','checked')
            }
        });

        let thisInput = $(this).find('input:checked');
        thisInput.removeAttr('checked')

        if (thisInput.next().length){
            thisInput.next().attr('checked','checked')
        }
        else{
            $(this).find('input').first().attr('checked','checked')
        }


        let clases = ['','sort-asc','sort-desc'];

        $(this).removeClass('sort-asc sort-desc')
        $(this).addClass(clases[parseInt($(this).find('input:checked').data('index'))])
    })


    $('.js_developer__list__order .developer__list__order__view').click(function(e){
        e.preventDefault()
        $(this).closest('.js_developer__list__order').toggleClass('active')
    })

    $('.js_modal_developer__list__order__mobile a').click(function(e){
        e.preventDefault()
        $('.js_modal_developer__list__order__mobile a').removeClass('active')
        $(this).addClass('active')
        $(this).closest('.js_developer__list__order').removeClass('active')
    })

    $('body').click(function (event) {
		if ((!$(event.target).closest('.js_developer__list__order').length && $('.js_developer__list__order').hasClass('active'))){
			$('.js_developer__list__order').removeClass('active')
		}
	})
});