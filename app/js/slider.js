$(document).ready(function () {
    function setBoundries(slick, state) {
        if (state === 'default') {
            slick.find('ul.slick-dots li').eq(3).addClass('n-small-1');
        }
    }

    // Slick Selector.
    var slickSlider = $('.js_slider__dots');
    var maxDots = 3;
    var transformXIntervalNext = -17;
    var transformXIntervalPrev = 17;

    slickSlider.on('init', function (event, slick) {
        $(this).find('ul.slick-dots').wrap("<div class='slick-dots-container'></div>");
        $(this).find('ul.slick-dots li').each(function (index) {
            $(this).addClass('dot-index-' + index);
        });
        $(this).find('ul.slick-dots').css('transform', 'translateX(0)');
        setBoundries($(this), 'default');
    });

    slickSlider.on('breakpoint', function(event, slick, breakpoint) {

        $(this).find('ul.slick-dots').wrap("<div class='slick-dots-container'></div>");
        $(this).find('ul.slick-dots li').each(function (index) {
            $(this).addClass('dot-index-' + index);
        });
        $(this).find('ul.slick-dots').css('transform', 'translateX(0)');
        setBoundries($(this), 'default');
    });



    var transformCount = 0;
    slickSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var totalCount = $(this).find('.slick-dots li').length;
        if (totalCount > maxDots) {
            if (nextSlide > currentSlide) {
                if ($(this).find('ul.slick-dots li.dot-index-' + nextSlide).hasClass('n-small-1')) {
                    if (!$(this).find('ul.slick-dots li:last-child').hasClass('n-small-1')) {
                        transformCount = transformCount + transformXIntervalNext;
                        $(this).find('ul.slick-dots li.dot-index-' + nextSlide).removeClass('n-small-1');
                        var nextSlidePlusOne = nextSlide + 1;
                        $(this).find('ul.slick-dots li.dot-index-' + nextSlidePlusOne).addClass('n-small-1');
                        $(this).find('ul.slick-dots').css('transform', 'translateX(' + transformCount + 'px)');
                        var pPointer = nextSlide - 2;
                        var pPointerMinusOne = pPointer - 1;
                        $(this).find('ul.slick-dots li').eq(pPointerMinusOne).removeClass('p-small-1');
                        $(this).find('ul.slick-dots li').eq(pPointer).addClass('p-small-1');
                    }
                }
            } else {
                if ($(this).find('ul.slick-dots li.dot-index-' + nextSlide).hasClass('p-small-1')) {
                    if (!$(this).find('ul.slick-dots li:first-child').hasClass('p-small-1')) {
                        transformCount = transformCount + transformXIntervalPrev;
                        $(this).find('ul.slick-dots li.dot-index-' + nextSlide).removeClass('p-small-1');
                        var nextSlidePlusOne = nextSlide - 1;
                        $(this).find('ul.slick-dots li.dot-index-' + nextSlidePlusOne).addClass('p-small-1');
                        $(this).find('ul.slick-dots').css('transform', 'translateX(' + transformCount + 'px)');
                        var nPointer = currentSlide + 2;
                        var nPointerMinusOne = nPointer - 1;
                        $(this).find('ul.slick-dots li').eq(nPointer).removeClass('n-small-1');
                        $(this).find('ul.slick-dots li').eq(nPointerMinusOne).addClass('n-small-1');
                    }
                }
            }
        }
    });


    $('.js_agent__slider__image').slick({
        arrows: false,
        slidesToShow: 1,
        dots: true,
        touchThreshold: 25,
        infinite: false,
        swipeToSlide: true,
        speed: 500,
    });



    $('.js_card_slider_image').slick({
        infinite: false,
        slidesToShow: 1,
        touchThreshold: 25,
        swipeToSlide: true,
        speed: 500,
        dots:true,
        responsive: [{
            breakpoint: 800,
            settings: {
                arrows:false,
                dots:true,
            }
        }]
    })


    $('.js_home__devaloper__list').slick({
        arrows: true,
        infinite: false,
        slidesToShow: 6,
        touchThreshold: 25,
        swipeToSlide: true,
        speed: 500,
        responsive: [{
                breakpoint: 1150,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 850,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 750,
                settings: {
                    variableWidth: true
                }
            },
        ]
    })








    $('.js_property__list').slick({
        arrows: true,
        infinite: true,
        touchThreshold: 25,
        swipeToSlide: true,
        speed: 500,
        variableWidth: true

    })


    $('.js_project__list').slick({
        arrows: true,
        infinite: true,
        touchThreshold: 25,
        swipeToSlide: true,
        speed: 500,
        variableWidth: true
    })





    $('.js_object_floor_plan__list').slick({
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        touchThreshold: 25,
        swipeToSlide: true,
        speed: 500,
        responsive: [{
                breakpoint: 901,
                settings: {
                    slidesToShow: 2,
                    dots:true
                }
            },
            {
                breakpoint: 551,
                settings: {
                    slidesToShow: 1,
                    dots:true
                }
            },
        ]
    })

});